var exec = require('cordova/exec');

const currency = {
    RUB: 'RUB',
};

Object.freeze(currency);
exports.currency = currency;

const errors = {
    BAD_DATA:           'BAD_DATA',
    BAD_CURRENCY:       'BAD_CURRENCY',
    BAD_AMOUNT:         'BAD_AMOUNT',
    BAD_NAME:           'BAD_NAME',
    BAD_DESCRIPTION:    'BAD_DESCRIPTION',
    BAD_CLIENT_APY_KEY: 'BAD_CLIENT_APY_KEY',
    BAD_SHOP_ID:        'BAD_SHOP_ID',
    PLUGIN_ERROR:       'PLUGIN_ERROR',
    BAD_METHODS_MASK:   'BAD_METHODS_MASK',
};

Object.freeze(errors);
exports.errors = errors;

const methods = {
    YANDEX_MONEY:   0b00001,
    BANK_CARD:      0b00010,
    SBERBANK:       0b00100,
    GOOGLE_PAY:     0b01000,
    APPLE_PAY:      0b10000
};

Object.freeze(methods);
exports.methods = methods;

exports.checkout = (data) => {
    return new Promise((resolve, reject) => {

        // Bad input object
        if(typeof data !== 'object' || data === null){
            return reject({
                error: errors.BAD_DATA,
                message: '',
            });
        }

        if(typeof data.methods !== 'number' || !(0b11111 & data.methods)){
            return reject({
                error: errors.BAD_METHODS_MASK,
                message: '',
            });
        }

        if(typeof data.amount !== 'number'){
            return reject({
                error: errors.BAD_AMOUNT,
                message: '',
            });
        }

        if(typeof Object.values(currency).find(currency => currency === data.currency) === 'undefined'){
            return reject({
                error: errors.BAD_CURRENCY,
                message: '',
            });
        }

        if(typeof data.name !== 'string'){
            return reject({
                error: errors.BAD_NAME,
                message: '',
            });
        }

        if(typeof data.description !== 'string'){
            return reject({
                error: errors.BAD_DESCRIPTION,
                message: '',
            });
        }

        if(typeof data.clientApiKey !== 'string'){
            return reject({
                error: errors.BAD_CLIENT_APY_KEY,
                message: '',
            });
        }

        if(typeof data.shopId !== 'string'){
            return reject({
                error: errors.BAD_SHOP_ID,
                message: '',
            });
        }

        const send = [parseInt(data.amount * 100), data.currency, data.name, data.description, data.clientApiKey, data.shopId, data.methods, !!data.debug, data.ios || ''];

        exec((token) => {
            resolve(token);
        }, (error) => {
            reject({
                error: errors.PLUGIN_ERROR,
                message: error,
            });
        }, 'YandexKassa', 'checkout', send);
    });
};

exports.secure = (url) => {
    return new Promise((resolve, reject) => {
        exec((status) => {
            resolve(status);
        }, (error) => {
            reject(error);
        }, 'YandexKassa', 'secure', [url]);
    });
};